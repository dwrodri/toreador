# TODOS 

## Maintenance/DevOps
- [ ] Set up CI/CD
- [ ] set up automatic updating of the version badge
- [ ] Set up some form of code coverage 
- [ ] Setup static type checking (with mypy?)
- [ ] Setup the GitLab Wiki
- [ ] Setup some popular alternative method of hosting documentation like Read The Docs

## Critical to entering alpha
- [ ] Get the rest of the pricing strategies set up
- [ ] WRITE MORE TESTS

## Stuff to do to get on PyPI
- [ ] make a PyPI account
- [ ] Make sure code has good test coverage
- [ ] write a setup.py
- [ ] test that I can setup my own package

## Features I think people would like
- [ ] A much better way of doing "risk free" interest 
- [ ] Migrating my code to a series of Jupyter Notebooks to host on a website
- [ ] Replace the standard class members with property objects when possible
- [ ] Create a portfolio evaluator that does some basic backtesting and/or Monte Carlo 
simulation.
- [ ] Get the repo mirrored over on GitHub, and EXPLICITLY FORBID PULL REQUESTS THERE
- [ ] Mock up some super simple frontend in Tkinter or something in TUI

