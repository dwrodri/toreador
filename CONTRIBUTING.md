For starters, thanks for reading this.
---
I use [black](https://github.com/ambv/black) with all of its defualt settings
for formatting all of the code. Here are some more notes on how I'd like this 
codebase formatted:

* If you have to bother with wrapping the class, you have to bother with 
documenting it. Please put the class descriptions in __init__(), unless the 
constructor is inherited.

* Use type annotations for all input parameters and value-returning 
functions. I'd like to have static type-checking in the CI/CD system (when it 
gets rolled out)

* Be consistent with variable names whenever possible. Current spot 
price should be s_0, the amount of time between option purchase and expiration 
is t, the strike price of an option is always k... and so forth. Most of these 
variable names come from a textbook, so they should be annotated throughout the 
documentation. However, don't be afraid to be brief with their descriptions.

* Write your code to handle the most complicated possible use cases, but 
never sacrifice the simplest possible use case you can imagine. For example, 
being able to pretend like I can go back in time and evaluate payoffs of using 
different pricing models is cool, but its less important than being able to 
instantiate an asset with a current price and just work from there

* Never add new code that requires any desktop environment or window manager. 
This is a calculation framework. If you want to use this code for your day 
trading adventures, and long for features like scraping 10-k forms or doing your
own technical analysis, then make our own repo and give credit where credit is 
due. 

* If you insist on breaking one of the earlier rules, explicitly state where in 
the code base you broke it and why. I'm not the smartest tool in the shed, I'm 
sure that some of my rules might be dumb to follow throughout the entire codebase. 
Hell, IDK if I follow them. 