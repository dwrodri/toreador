from .asset import Asset
from .interest import future_value as fv
from abc import abstractmethod, ABC
from numbers import Number
from typing import List
from datetime import timedelta


class Option(ABC):
    def __init__(
        self, k: Number, t: timedelta, cost: Number, asset: Asset, is_euro: bool = True
    ):
        """The superclass defining common option behavior
 
        Args:
            k: strike price of the option (i.e. the agreed money to change hands 
               at enforcement time)
            t: time until option expires (in years)
            cost: the amount of money required to buy this option
            asset: the underlying asset on which the option is based
            is_euro: whether the option can be exercised or not
        """
        self._k = k
        self._t = t
        self._cost = cost
        self._asset = asset
        self._is_euro = is_euro

    @abstractmethod
    def eval_payoff(self, s_t: Number, written: bool = False) -> Number:
        """Given a strike price of the asset, evaluate the payoff of this
        option. Automatically accounts for whether it is more profitable to 
        exercise or not.
        Args:
            s_t: the spot price, i.e. market price of the asset
            written: whether to evaluate the value of the asset from the 
                perspective of the person who wrote/sold the option or the 
                person who bought it
        
        Returns:
           A Number (float or int) representing the money that is traded hands at
           the exercise time. 
        """
        pass


class Call(Option):
    def eval_payoff(self, s_t: Number, written: bool = False) -> Number:
        """Return the amount of money gained from exercising the call.

        Calculate and return the amount of money that changes hands when the 
        call is exercised or expires. In this case, gaining a negative amount 
        of money implies that you lost money in the exchange. 
        money.
        """
        if written:  # option purchaser won't exercise if self._k > s_t
            money_from_exercise = s_t - self._k if s_t < self._k else 0
            return fv(self._cost) - money_from_exercise
        else:
            return max(-fv(self._cost), s_t - self._k)


class Put(Option):
    def eval_payoff(self, s_t: Number, written: bool = False) -> Number:
        """Return the value of money gained from option.

        A negative value results from getting less from exercising. Again, see
        option superclass for variable info.
        """
        if written:
            return max(fv(self._cost), s_t - self._k)
        else:
            return max(-fv(self._cost), self._k - s_t - fv(self._cost))


class CombinedOptionStrategy:
    def __init__(self, opts: List[Option]):
        """A wrapper class for conveniently evaluating groups of options.

        Args:
            opts: list of Option objects
        """
        self._opts = opts
