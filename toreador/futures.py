from .interest import future_value as fv
from .interest import RISK_FREE_RATE
from .asset import Asset
from numbers import Number
from datetime import timedelta


class Future:
    def __init__(
        self,
        k: Number,
        asset: Asset,
        t: timedelta = timedelta(days=365),
        t_0: datetime = datetime.now(),
    ):
        """A future is a promise that an asset will be sold at a certain price.

        Futures are a type of derivative that guarantees a certain amount of an asset 
        to be sold at a given pre-determined price at a certain time in the future. 
        Futures also specify the buying and selling parties ahead of time. 

        Args:
            k: the agreed strike price to be enforced in the future
            Asset: the underlying asset upon which the forward is made 
            t: the amount of time between now and the enforcement
            t_0: the datetime when the future was created.
        """
        self._k = k
        self._asset = asset
        self._t = t
        self._t_0 = t_0

    def eval_payoff(self, s_t: Number, rate=RISK_FREE_RATE, short=False):
        """Calculate the payoff for a given party on the future.
        
        Args:
            s_t: the spot price for the asset at enforcement
            rate: interest rate at which money is growing
            short: determine whether payoff is calculated for the buyer or the seller
        """
        cost_of_future = fv(self._asset, rate=rate, t=t) - self._asset._get_price(
            self._t_0
        )

        if short:  # if you agreed to sell the asset
            return self._k - s_t - cost_of_future
        else:
            return s_t - cost_of_future - k
