from .options import Put, Call, Option
from .asset import Asset
from .interest import RISK_FREE_RATE
from .interest import present_value as pv
from .interest import future_value as fv
from datetime import timedelta
from numbers import Number


def put_call_parity(priced_opt: Option, rate: float = RISK_FREE_RATE) -> float:
    """Calculate the price of a european option using another same-class option.

    Put-Call Parity pricing strategy is designed to eliminate the arbitrage that is 
    created by creating synthetic forwards (short put + long call). To eliminate this 
    arbitrage, the following equation is used:

                                C - P = PV(k) - S_0

    Where C = price of a call, P = price of a put, k is the strike price for both 
    options, PV() is a function that discounts a forward value of money ot its present 
    value and S_0 is current market value for the asset. So in simple terms, you 
    shouldn't be able to combine options to make free money. 

    Args:
        priced_opt = one pre-priced option on an asset. This is of course, assumed to 
            be already "fairly priced".
        rate = the interest rate to be used for the present_value function

    Returns:
        A fair price for the matching option. If the function is passed a call, it 
        will automagically return the price of a put, and vice versa. 
    """
    if type(priced_opt) is Call:
        return (
            pv(priced_opt._k, priced_opt._t, rate)
            - priced_opt._asset.get_price()
            + priced_opt._cost
        )
    else:
        return present_strike - priced_opt._asset.get_price() + priced_opt._cost

def simple_binomial_tree(
    t: timedelta,
    s_l: Number,
    s_h: Number,
    s_0: Number,
    k: Number,
    rate: Number = RISK_FREE_RATE,
) -> float:
    """Use a simple binomial tree to calculate fair cost of option.

    Calculate risk-free probability given a perfectly known low and higher bound for 
    the price of an asset such that the following equation holds:

                            FV(S_0) = p * S_h + (1-p) * S_l

    Where FV() calculates the future value of an amount of money, S_h is the highest 
    possible outcome and S_l is the lowest possible outcome. The binomial pricing model
    assumes that both the high and the low are equally likely to happen. To finally 
    calculate the cost of the option you then use the solved probability p and 
    compute the following: 

          Cost of option  = p * (payoff if S_H occurs) + (1-p) * (payoff if S_l occurs)

    In a few words, that means the cost of the option is the weighted average of the 
    most extreme outcomes.

    Args:
        t: amount of time between option purchase and expiration
        s_l: the lowest possible spot price for the asset at option expiration
        s_h: the highest possible spot price for the asset at option expiration
        s_0: current spot price
        rate: interest rate for continuous compunding

    Returns:
        A fair price based on the assumption that the future price of the asset will 
        be either exactly S_l or S_h where both outcomes have equal probability
    """
    p = (fv(s_0, rate=rate, t=timedelta) - s_l) / (s_h - s_l)
    return s_h * p + s_l * (1 - p)


def binomial_tree_with_forward_prices(
    t: timedelta,
    sigma: Number,
    s_0: Number,
    k: Number,
    delta: Number = 0.0,
    rate: Number = RISK_FREE_RATE,
):
    """Build a binomial tree based on the volatility of an option.

    Binomial trees can be constructed based on the volatility of an option. I'll go 
    ahead and do an extensive write-up on this when the function gets fully 
    fully implemented. 

    Args:
        t: amount of time between option purchase and expiration
        sigma: volatility of the asset, can equate to the variance in the price
        s_0: current market price of the asset
        k: strike price of the option
        rate: current rate of interest

    Returns:
        The price of the option based on the binomial tree generated from the 
        volatility. 
    """
    spread = sigma * sqrt(t)
    down_scaler = exp((rate - delta) * t - spread)  # this is "d" in literature
    up_scaler = exp((rate - delta) * t + spread)  # and this is "u"
    return simple_binomial_tree(t, down_scaler * s_0, up_scaler * s_0, k, rate)


def simple_black_scholes():
    pass
