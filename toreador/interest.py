from math import exp, log
from numbers import Number
from datetime import timedelta

# A simple reference "risk-free" interest rate as a global variable. This should be
# set according to the Treasury Bill rates. A T-Bill is an IOU issued by the U.S.
# Treasury for its national debt. The value set here is the 1 year rate that was set
# on October 11th, 2018.
#
# TODO: Implement some functionality for scraping Treasury yield rates from the
# Dept. of Treasury's website. That would probably go in own module`
RISK_FREE_RATE = 0.026

# TODO: Guarantee that leap years are accounted for. I think I've mitigated this to
# a certain extent by using the timedelta object.
DAYS_IN_A_YEAR = 365


def percent_growth_to_continuous_rate(percent: float) -> float:
    """convert delta percentage to the r value in e^(r*T) for continuous interest"""
    return log(1 + (percent / 100))


def future_value(
    principle: Number, t: timedelta = timedelta(days=365), rate: Number = RISK_FREE_RATE
) -> float:
    """Calculate the expected future value of a given amount

    Args:
    principle: the base value of money in the present
    t: the amount of time the money is allowed to grow
    rate: the rate at which the money is growing, defaults to the risk-free rate

    Returns:
    The future value using exponential growth
    """
    global DAYS_IN_A_YEAR
    return principle * exp(rate * (t.days / DAYS_IN_A_YEAR))


def present_value(
    principle: Number, t: timedelta = timedelta(days=365), rate: Number = RISK_FREE_RATE
) -> float:
    """Take a future value of money and discount it back to the present

    Args:
    principle: the base value of money in the present
    t: the amount of time the money is allowed to grow
    rate: the rate at which the money is growing, defaults to the risk-free rate

    Returns:
    A future value discounted to the present time
    """
    global DAYS_IN_A_YEAR
    return principle * exp(-rate * (t.days / DAYS_IN_A_YEAR))
