from .interest import future_value as fv
from .interest import DAYS_IN_A_YEAR
from numbers import Number
from datetime import datetime, timedelta
from collections import Iterable, OrderedDict
from typing import Any, Dict, List, Tuple, Union
from math import exp


class Asset:
    def __init__(
        self,
        name: str = "VOC",
        amount: Number = 1,
        price_history: Dict[datetime, Number] = None,
        s_0: Number = None,
    ):
        """A container class for holding asset data.
    
        An asset is literally  anything of value that can be traded on an 
        exchange. Use this class to represent commodities and anything that will never 
        pay dividends, or split, or do anything like that.

        Args:
            name: the name of the asset. The default is a name referring to the 
                Dutch East India Company. 
            price_history: a dictionary containing some historical data for the
                price of the asset
            s_0: the current market price for the good. Use this if you're doing 
                simple calculations instead of forecasting from a previous point
                in time
        """
        self._name = name
        self._amount = amount
        if price_history:
            self._price_history = price_history
        elif s_0:
            self._price_history = {datetime.now(): s_0}

    def __str__(self):
        output = "Name:\t" + self._name + " | "
        output += "S_0: " + str(self.get_price()) + " | "
        return output

    def __repr__(self):
        return self.__str__()

    def get_price(self, current_time: datetime = datetime.now()) -> Number:
        """return value of the asset at given point in time.

        This is mainly a wrapper function for simplifying working with the Asset class 
        (for now), since I won't be implementing historical price scraping any time 
        soon.

        Args:
            current_time: key used to search for the most recent known price in the
                price_history dict

        Returns:
            the nearest previously known price of the asset
        """
        global DAYS_IN_A_YEAR
        try:
            return self._amount * self._price_history[current_time]
        except KeyError:
            for key in self._price_history.keys():
                if (current_time - key).days / DAYS_IN_A_YEAR < 0:
                    return self._amount * self._price_history[key]


class ContinuousDividendAsset(Asset):
    def __init__(
        self,
        name: str = "VOC",
        amount: Number = 1,
        price_history: Dict[datetime, Number] = None,
        s_0: Number = None,
        dividend_rate: Number = 0.0,
    ):
        """Used to represent assets that grow at a continuous rate
    
        Some companies and index funds will have the shareholders stake grow 
        continuously. This needs to be accounted for when calculating the cost of 
        derivatives on those assets. See Asset class for inherited arguments

        Args:
            dividend_rate: the rate at which the asset is growing.
        """
        self._name = name
        self._amount = amount
        self._dividend_rate = dividend_rate
        if price_history:
            self._price_history = price_history
        elif s_0:
            self._price_history = {datetime.now(): s_0}

    def get_dividend_value(self, t: timedelta) -> float:
        """ Calculate the monetary value of the dividends that acccumulate over t.
        
        Args:
            t: the amount of time over which wee are calculating the dividends

        Returns:
            the monetary value of the dividends that accumulated over time
        """
        return fv(
            principle=self._amount * self.get_current_price(),
            rate=self._dividend_rate,
            t=t,
        ) - (self._amount * self.get_price)
