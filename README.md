# toreador
<p align = "center">
<a href="https://semver.org/"><img alt ="Unstable alpha" src="https://img.shields.io/badge/version-0.0.1--unstable-red.svg"></a>
<a href="https://github.com/ambv/black"><img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-000000.svg"></a>
</p>

The goal of this project is to be an educational tool for getting programmers into 
options pricing. I'd like it to be more friendly for people just getting started with 
options, and more platform agnostic. It will be easy for you to plug in your own data,
 but I wouldn't be opposed to integrating some basic scraping tools into the framework 
as well. Here's a basic outline of what I'd like to start with for this project:

 
## TODOS
See the [checklist](TODOS.md)


## Contributing
See the [guide](CONTRIBUTING.md).

