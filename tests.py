from toreador.options import Put
from toreador.options import Call
from toreador.pricing import put_call_parity
from toreador.interest import future_value as fv
from toreador.interest import present_value as pv
from toreador import Asset
from datetime import datetime, timedelta
import unittest


class ToreadorTests(unittest.TestCase):
    def test_put_call_parity(self):
        stock = Asset(s_0=30)
        call = Call(k=25, cost=8.05, t=timedelta(days=365), asset=stock)
        put_cost = put_call_parity(call, rate=0.05)
        self.assertTrue(
            call._cost - put_cost
            == - (pv(call._k, rate=0.05) - stock.get_price())
        )


if __name__ == "__main__":
    unittest.main()
